import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(recipient,body):
    sender_email = "2ec8e9c4086bd0"
    receiver_email = recipient
    password = "8287c393b824ff"

    message = MIMEMultipart("alternative")
    message["Subject"] = "Export Complete!"
    message["From"] = sender_email
    message["To"] = receiver_email

    html = body
    # Create secure connection with server and send email
    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)
    with smtplib.SMTP("smtp.mailtrap.io", 465) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )