# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""
from flask import g
import mysql.connector
import requests
import json
from flask import jsonify, render_template, redirect, request, url_for
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user
)

from app import db, login_manager, mysqldb
from app.base import blueprint
from app.base.forms import LoginForm, CreateAccountForm, CreateConnectionForm
from app.base.models import User, DBConnection, Report,InternalUser,Notification

from app.base.util import verify_pass
import pymongo
import urllib
from bson.json_util import dumps, loads
import re
import pandas as pd
import spacy
from spacy import displacy
import en_core_web_sm
nlp = spacy.load('en_core_web_sm')
import numpy as np
import pandas as pd
import jwt
from app.base.mail import send_email


def get_mysql_db(credentials):
    print("credentials")
    print(credentials)
    mysqldb = g.get('_db', None)
    if mysqldb is None:
        print("not found in context")
        mysqldb = connect_to_mysql_database(credentials)
        print(mysqldb)
        g._db = mysqldb
        # print(g.get('_db'))
    return mysqldb


def connect_to_mysql_database(credentials):
    mydb = mysql.connector.connect(
        host=credentials['host'],
        user=credentials['user'],
        password=credentials['pwd'],
        # database=credentials['db']
    )
    return mydb


@blueprint.route('/')
def route_default():
    return redirect(url_for('base_blueprint.login'))


@blueprint.route('/error-<error>')
def route_errors(error):
    return render_template('errors/{}.html'.format(error))

# Login & Registration


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    if 'login' in request.form:

        # read form data
        username = request.form['username']
        password = request.form['password']

        # Locate user
        user = User.query.filter_by(username=username).first()

        # Check the password
        if user and verify_pass(password, user.password):

            login_user(user)
            return redirect(url_for('base_blueprint.route_default'))

        # Something (user or pass) is not ok
        return render_template('accounts/login.html', msg='Wrong user or password', form=login_form)

    if not current_user.is_authenticated:
        return render_template('accounts/login.html',
                               form=login_form)
    return redirect(url_for('home_blueprint.index'))


@blueprint.route('/login-new', methods=['GET', 'POST'])
def login_new():
    username = request.json['username']
    password = request.json['password']

    # Locate user
    user = User.query.filter_by(username=username).first()

    # Check the password
    if user and verify_pass(password, user.password):
        login_user(user)
        return jsonify({'status': 1})
    else:
        return jsonify({'status': 0})


@blueprint.route('/register', methods=['GET', 'POST'])
def register():
    login_form = LoginForm(request.form)
    create_account_form = CreateAccountForm(request.form)
    if 'register' in request.form:

        username = request.form['username']
        email = request.form['email']
        print(username)
        print(email)
        # Check usename exists
        user = User.query.filter_by(username=username).first()
        if user:
            return render_template('accounts/register.html',
                                   msg='Username already registered',
                                   success=False,
                                   form=create_account_form)

        # Check email exists
        user = User.query.filter_by(email=email).first()
        if user:
            return render_template('accounts/register.html',
                                   msg='Email already registered',
                                   success=False,
                                   form=create_account_form)

        # else we can create the user
        user = User(**request.form)
        db.session.add(user)
        db.session.commit()

        return render_template('accounts/register.html',
                               msg='User created please <a href="/login">login</a>',
                               success=True,
                               form=create_account_form)

    else:
        return render_template('accounts/register.html', form=create_account_form)


@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('base_blueprint.login'))


@blueprint.route('/shutdown')
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'

# Errors


@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template('page-403.html'), 403


@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template('page-403.html'), 403


@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template('page-404.html'), 404


@blueprint.errorhandler(500)
def internal_error(error):
    return render_template('page-500.html'), 500


@blueprint.route('/snow', methods=['GET', 'POST'])
def snow():
    user = request.json['username']
    pwd = request.json['password']
    url = 'https://dev86307.service-now.com/api/now/table/incident?sysparm_limit=10'
    # Set proper headers
    headers = {"Content-Type": "application/json",
               "Accept": "application/json"}
    # Do the HTTP request
    response = requests.get(url, auth=(user, pwd), headers=headers)

    # Check for HTTP codes other than 200
    if response.status_code != 200:
        print('Status:', response.status_code, 'Headers:',
              response.headers, 'Error Response:', response.body)
        exit()

    # Decode the JSON response into a dictionary and use the data
    data = response
    # print(data.headers['content-type'])
    return jsonify(data.json())


@blueprint.route('/mongo', methods=['GET', 'POST'])
def mongo():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    #   cluster0.fyvoa.mongodb.net
    mongo_uri = "mongodb://"+user+":" + \
        urllib.parse.quote(pwd) + "@"+host
    print(mongo_uri)
    client = pymongo.MongoClient(mongo_uri)
    # db = client.insights
    # cursor = db.cumulative_returns_portfolio_request.find({}, {"_id": 0})
    dbs = client.list_database_names()
    dbs = list(map(lambda x: {'db': x}, dbs))
    return jsonify(dbs)


@blueprint.route('/mysql', methods=['GET', 'POST'])
def connect_mysql():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    mydb = get_mysql_db({'host': host, 'user': user,
                         'pwd': pwd, 'con_name': request.json['con_name']})
    mycursor = mydb.cursor()
    mycursor.execute("SHOW DATABASES")
    # db = client.insights
    # cursor = db.cumulative_returns_portfolio_request.find({}, {"_id": 0})
    dbs = list(map(lambda x: {'db': x[0]}, mycursor))
    return jsonify(dbs)


@blueprint.route('/mongo-collection-list', methods=['GET', 'POST'])
def mongo_collection_list():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    db = request.json['db']
    #   cluster0.fyvoa.mongodb.net
    # mongo_uri = "mongodb+srv://"+user+":" + \
    #     urllib.parse.quote(pwd) + "@"+host + \
    #     "/insights?retryWrites=true&w=majority"
    mongo_uri = "mongodb://"+user+":" + \
        urllib.parse.quote(pwd) + "@"+host
    client = pymongo.MongoClient(mongo_uri)
    db = client[db]
    # cursor = db.cumulative_returns_portfolio_request.find({}, {"_id": 0})
    return jsonify(db.list_collection_names())


@blueprint.route('/mysql-table-list', methods=['GET', 'POST'])
def mysql_table_list():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    db = request.json['db']
    mydb = get_mysql_db({'host': host, 'user': user,
                         'pwd': pwd, 'con_name': request.json['con_name']})
    mycursor = mydb.cursor()
    mycursor.execute("USE "+db)
    mycursor.execute("SHOW TABLES")
    return jsonify(list(map(lambda x: x[0], mycursor)))


@blueprint.route('/mongodata', methods=['GET', 'POST'])
def mongodata():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    db = request.json['db']
    col = request.json['col']
    #   cluster0.fyvoa.mongodb.net
    # mongo_uri = "mongodb+srv://"+user+":" + \
    #     urllib.parse.quote(pwd) + "@"+host + \
    #     "/insights?retryWrites=true&w=majority"
    mongo_uri = "mongodb://"+user+":" + \
        urllib.parse.quote(pwd) + "@"+host
    client = pymongo.MongoClient(mongo_uri)
    db = client[db]
    cursor = db[col].find({}, {"_id": 0})
    return jsonify(list(cursor))


@blueprint.route('/export-to-mongo', methods=['GET', 'POST'])
def export_to_mongo():
    export_from = request.json['export_from']
    export_to = request.json['export_to']
    export_info = request.json['export_to']['export_info'] 
    exported_data = []
    secret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
    if export_from.get('type', -1) == 1:
        exported_data = get_mysql_data(host=export_from['host'], user=export_from['username'], pwd=export_from['password'], db=export_from['db'],
                       col=export_from['col'], filters=export_from['filters'], transformer=export_from['transformer'], con_name=export_from['con_name'])
    
    exported_data = transformExportData(exported_data,export_info,secret)
    #   cluster0.fyvoa.mongodb.net
    user = export_to['username']
    host = export_to['host']
    pwd = export_to['password']
    db = export_to['db']
    col = export_to['col']

    # mongo_uri = "mongodb+srv://"+user+":" + \
    #     urllib.parse.quote(pwd) + "@"+host + \
    #     "/insights?retryWrites=true&w=majority"
    mongo_uri = "mongodb://"+user+":" + \
        urllib.parse.quote(pwd) + "@"+host
    client = pymongo.MongoClient(mongo_uri)
    db = client[db]
    cursor = db[col].insert_many(exported_data)
    resp = {
        'status' : 0
    }
    if len(cursor.inserted_ids) > 0:
        resp['status'] = 1
    
    records = Notification.query.filter_by(name = 'EXPORT')
    recipient = ''
    body = ''
    for r in records:
        recipient = r.recipients
        body = r.body.replace('$key',secret)
    send_email(recipient,body)
    return jsonify(resp)

def transformExportData(exported_data,export_info,secret):
    if export_info['name']:
        for _ex_data in exported_data:
            for col in export_info['cols']:
                _ex_data[col] = jwt.encode({'value' : _ex_data[col]}, secret, algorithm=export_info['name'])
    return exported_data
def get_mysql_data(host, user, pwd, db, col, filters, transformer, con_name):
    mydb = get_mysql_db(
        {
            'host': host,
            'user': user,
            'pwd': pwd,
            'con_name': con_name
        }
    )
    mycursor = mydb.cursor(dictionary=True)
    mycursor.execute("USE "+db)
    transformerStrList = []
    transformerStr = ''
    if transformer:
        for t in transformer:
            if t['op'] == 1:
                transformerStrList.append(
                    ' + '.join(t['targets']) + ' as ' + t['result'])
            elif t['op'] == 2:
                transformerStrList.append(
                    'CONCAT('+(','.join(t['targets'])) + ') as ' + t['result'])
            elif t['op'] == 3:
                transformerStrList.append(
                    ' * '.join(t['targets']) + ' as ' + t['result'])
            elif t['op'] == 4:
                transformerStrList.append(
                    ' - '.join(t['targets']) + ' as ' + t['result'])
        transformerStr = ' , '.join(transformerStrList)
        print(transformerStr)

    if filters and len(filters) > 0:
        filterStrList = []
        for filter in filters:
            filterStrList.append(
                filter['field'] + filter['condition'] + '\''+filter['val'] + '\'')
        filterStr = " AND ".join(filterStrList)
        if len(transformerStr) != 0:
            mycursor.execute("SELECT * ," + transformerStr +
                             " FROM "+col + " where " + filterStr+"  LIMIT 50")
        else:
            mycursor.execute("SELECT *  FROM "+col +
                             " where " + filterStr+"  LIMIT 50")
        myresult = mycursor.fetchall()
    else:
        if len(transformerStr) != 0:
            mycursor.execute("SELECT * ," + transformerStr +
                             " FROM "+col + " LIMIT 50")
        else:
            mycursor.execute("SELECT * FROM "+col + " LIMIT 50")
        myresult = mycursor.fetchall()
    # myresult = dict(zip(mycursor.column_names, mycursor.fetchall()))

    return myresult


@blueprint.route('/mysqldata', methods=['GET', 'POST'])
def mysqldata():
    print("request object")
    print(request.json)
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    db = request.json['db']
    col = request.json['col']
    filters = request.json.get('filters', [])
    transformer = request.json.get('transformer', [])
    #   cluster0.fyvoa.mongodb.net
    data = get_mysql_data(host=host, user=user, pwd=pwd, db=db, col=col, filters=filters,
                   transformer=transformer, con_name=request.json['con_name'])
    
    # return jsonify({'analytics' :  mysqldataanalytics(data) , 'data' : data})
    return jsonify(data)

@blueprint.route('/mysql-data-analytics', methods=['GET', 'POST'])
def mysqldataanalytics():
    data = request.json['data']
    cols = request.json['cols']
    df = pd.json_normalize(data)
    response = []
    for col in cols:
        desc = df[col].describe()
        response.append({'col' : col,'desc' : json.loads(desc.to_json())})
    return jsonify(response)


@blueprint.route('/ner_process', methods=['POST'])
def process():
    if request.method == 'POST':
        choice = request.form['taskoption']
        rawtext = request.form['rawtext']
        doc = nlp(rawtext)
        d = []
        for ent in doc.ents:
            d.append((ent.label_, ent.text))
            df = pd.DataFrame(d, columns=('named entity', 'output'))
            ORG_named_entity = df.loc[df['named entity'] == 'ORG']['output']
            PERSON_named_entity = df.loc[df['named entity']
                                         == 'PERSON']['output']
            GPE_named_entity = df.loc[df['named entity'] == 'GPE']['output']
            MONEY_named_entity = df.loc[df['named entity']
                                        == 'MONEY']['output']
        if choice == 'organization':
            results = ORG_named_entity
            num_of_results = len(results)
        elif choice == 'person':
            results = PERSON_named_entity
            num_of_results = len(results)
        elif choice == 'geopolitical':
            results = GPE_named_entity
            num_of_results = len(results)
        elif choice == 'money':
            results = MONEY_named_entity
            num_of_results = len(results)

    return render_template("bc_ner.html", results=results, num_of_results=num_of_results)


@blueprint.route('/add-connection', methods=['POST'])
def add_connection():

    user = DBConnection(request.json)
    print("user")
    print(user)

    db.session.add(user)
    db.session.commit()
    # return {'status' : 1}
    return jsonify(user)

# @blueprint.route('/update-connection', methods=['POST'])
# def add_connection():
#     user = DBConnection(request.json)
#     print("user")
#     print(user)

#     db.session.update(user)
#     db.session.commit()
#     # return {'status' : 1}
#     return jsonify(user)

@blueprint.route('/add-user', methods=['POST'])
def add_user():
    user = InternalUser(request.json)
    print(user)

    db.session.add(user)
    db.session.commit()
    # return {'status' : 1}
    return jsonify(user)

@blueprint.route('/add-notification', methods=['POST'])
def add_notification():
    notification = Notification(request.json)
    print(notification)

    db.session.add(notification)
    db.session.commit()
    # return {'status' : 1}
    return jsonify(notification)


@blueprint.route('/add-report', methods=['POST'])
def add_report():

    report = Report(request.json)
    print("report")
    print(report)

    db.session.add(report)
    db.session.commit()
    # return {'status' : 1}
    return jsonify(report)


@blueprint.route('/get-connection-list', methods=['GET'])
def get_connection_list():
    args = request.args
    records = DBConnection.query.filter_by(con_type=args['type'])
    data = []
    for record in records:
        data.append({
            'id':  record.id,
            'con_name': record.con_name,
            'host': record.host,
            'username': record.username,
            'password': record.password
        })
    return jsonify(data)

@blueprint.route('/get-user-list', methods=['GET'])
def get_user_list():
    records = InternalUser.query.filter_by()
    data = []
    for record in records:
        data.append({
            'id':  record.id,
            'fullname': record.full_name,
            'email': record.email,
            'phone': record.phone
        })
    return jsonify(data)


@blueprint.route('/get-notification-list', methods=['GET'])
def get_notification_list():
    records = Notification.query.filter_by()
    data = []
    for record in records:
        data.append({
            'id':  record.id,
            'name': record.name,
            'recipients': record.recipients,
            'body': record.body
        })
    return jsonify(data)


@blueprint.route('/get-report-list', methods=['GET'])
def get_report_list():
    records = Report.query.all()
    data = []
    for record in records:
        data.append({
            'id':  record.id,
            'name': record.name,
            'con_id': record.con_id,
            'last_updated': record.last_updated
        })
    return jsonify(data)


@blueprint.route('/mysql-analytics', methods=['GET', 'POST'])
def mysql_analytics():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    db = request.json['db']
    col = request.json['col']
    analytics = request.json['analytics']
    #   cluster0.fyvoa.mongodb.net
    mydb = mysql.connector.connect(
        host=host,
        user=user,
        password=pwd,
        database=db
    )
    mycursor = mydb.cursor(dictionary=True)
    if analytics:
        mycursor.execute("SELECT COUNT(*) as value," +
                         analytics['x']+" as name FROM "+col + " GROUP BY "+analytics['x'])
        myresult = mycursor.fetchall()
    else:
        return jsonify([])
    # myresult = dict(zip(mycursor.column_names, mycursor.fetchall()))

    return jsonify(myresult)


@blueprint.route('/mysql-cols', methods=['POST'])
def get_mysql_cols_metadata():
    host = request.json['host']
    user = request.json['username']
    pwd = request.json['password']
    db = request.json['db']
    col = request.json['collection']
    mydb = get_mysql_db(
        {
            'host': host,
            'user': user,
            'pwd': pwd,
            'con_name': request.json['con_name']
        }
    )
    mycursor = mydb.cursor(dictionary=True)
    mycursor.execute("USE information_schema")
    # mycursor.fetchall() 
    print("SELECT COLUMN_NAME,DATA_TYPE FROM COLUMNS WHERE TABLE_NAME =  '"+col+"'")
    mycursor.execute(
        "SELECT COLUMN_NAME,DATA_TYPE FROM COLUMNS WHERE TABLE_NAME =  '"+col+"'")

    return jsonify(mycursor.fetchall())
