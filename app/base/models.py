# -*- encoding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 - present AppSeed.us
"""
from dataclasses import dataclass
from flask_login import UserMixin
from sqlalchemy import Binary, Column, Integer, String,JSON,DateTime

from app import db, login_manager

from app.base.util import hash_pass

from datetime import datetime

class User(db.Model, UserMixin):

    __tablename__ = 'User'

    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True)
    email = Column(String, unique=True)
    password = Column(Binary)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            # depending on whether value is an iterable or not, we must
            # unpack it's value (when **kwargs is request.form, some values
            # will be a 1-element list)
            if hasattr(value, '__iter__') and not isinstance(value, str):
                # the ,= unpack of a singleton fails PEP8 (travis flake8 test)
                value = value[0]

            if property == 'password':
                value = hash_pass(value)  # we need bytes here (not plain str)

            setattr(self, property, value)

    def __repr__(self):
        return str(self.username)


@login_manager.user_loader
def user_loader(id):
    return User.query.filter_by(id=id).first()


@login_manager.request_loader
def request_loader(request):
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    return user if user else None

@dataclass
class DBConnection(db.Model):

    __tablename__ = 'DBConnection'

    id = Column(Integer, primary_key=True, autoincrement=True)
    con_name = Column(String, unique=True)
    host = Column(String)
    username = Column(String)
    password = Column(String)
    con_type = Column(String)

    def __init__(self, json):
        self.con_name = json['conname']
        self.host = json['host']
        self.username = json['username']
        self.password = json['password']
        self.con_type = json['type']

    def __repr__(self):
        return str({
            'con_name': self.con_name,
            'host':  self.host,
            'username':  self.username,
            'password':  self.password,
            'con_type':  self.con_type            
        })

@dataclass
class Report(db.Model):

    __tablename__ = 'Report'

    id = Column(Integer, primary_key=True, autoincrement=True)
    con_id = Column(Integer)
    filters = Column(JSON)
    transformer = Column(JSON)
    name = Column(String)
    last_updated =  Column(DateTime, default=datetime.utcnow())

    def __init__(self, json):
        self.con_id = json['con_id']
        self.filters = json['filters']
        self.transformer = json.get('transformer',{})
        self.name = json['name']

    def __repr__(self):
        return str({
            'con_id': self.con_id,
            'filters':  self.filters,   
            'transformer':  self.transformer,   
            'name':  self.name     
        })

@dataclass
class InternalUser(db.Model):

    __tablename__ = 'InternalUser'

    id = Column(Integer, primary_key=True, autoincrement=True)
    full_name = Column(String, unique=True)
    email = Column(String)
    phone = Column(String)
    
    def __init__(self, json):
        self.full_name = json['full_name']
        self.email = json['email']
        self.phone = json['phone']
        

    def __repr__(self):
        return str({
            'full_name': self.full_name,
            'email':  self.email,
            'phone':  self.phone,
            
        })

@dataclass
class Notification(db.Model):

    __tablename__ = 'Notification'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True)
    recipients = Column(String)
    body = Column(String)
    
    def __init__(self, json):
        self.name = json['name']
        self.recipients = json['recipients']
        self.body = json['body']
        

    def __repr__(self):
        return str({
            'name': self.name,
            'recipients':  self.recipients,
            'body':  self.body,
            
        })
