# -*- encoding: utf-8 -*-
"""
Developer : Anil Chaudhary
Copyright (c) 2020 - 161dev.com
"""

from flask_migrate import Migrate
from os import environ
from sys import exit
from decouple import config

from config import config_dict
from app import create_app, db
from flask_cors import CORS

# WARNING: Don't run with debug turned on in production!
DEBUG = config('DEBUG', default=True)

# The configuration
get_config_mode = 'Debug' if DEBUG else 'Production'

try:
    
    # Load the configuration using the default values 
    app_config = config_dict[get_config_mode.capitalize()]

except KeyError:
    exit('Error: Invalid <config_mode>. Expected values [Debug, Production] ')


app = create_app( app_config )
CORS(app)	

app.config.update(dict(
    SECRET_KEY="powerful_secretkey",
    WTF_CSRF_SECRET_KEY="a_csrf_secret_key"
)) 
Migrate(app, db)

if __name__ == "__main__":
    app.run(port=5000,host="0.0.0.0",debug=True)
